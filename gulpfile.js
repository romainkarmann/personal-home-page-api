const gulp = require('gulp');
const babel = require('gulp-babel');
const htmlmin = require('gulp-htmlmin');

gulp.task('build', () => {
  return gulp.src('./src/**/*.js')
    .pipe(babel())
    .pipe(htmlmin())
    .pipe(gulp.dest('./deploy'));
});
